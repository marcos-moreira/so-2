
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author avilapm
 */
public class App {
    public static void main(String[] args) throws InterruptedException{
    
           
           ExecutorService executor = Executors.newCachedThreadPool();
           
           for(int i=0; i<200;i++){
               executor.submit(() -> {
                   Connection.getInstance().connect();
               });
           }
         
           executor.shutdown();
           executor.awaitTermination(1, TimeUnit.DAYS);
    }
    /* abre barbearia ao clicar no botao start e gera essas 200 threads */
    
}
