/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package philosophers.dinner;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 *
 * @author root
 */
public class Garfo {

    public AtomicBoolean flag = new AtomicBoolean(false);
    int id;

    public Garfo(int id) {
        this.id = id;
    }

    public synchronized void pegar() throws InterruptedException {
        while (this.flag.get()) {
            this.wait();
        }

        System.out.println("Pegou garfo " + id);
        flag.set(true);
    }

    public synchronized void devolver() {

        flag.set(false);

        notifyAll();;
    }
}
