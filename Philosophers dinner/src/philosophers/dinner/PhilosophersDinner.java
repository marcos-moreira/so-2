package philosophers.dinner;

import java.util.LinkedList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 * @author root
 */
public class PhilosophersDinner {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        LinkedList<Garfo> garfos = new LinkedList<>();
        LinkedList<Filosofo> filosofos = new LinkedList<>();

        Garfo garfo;
        Filosofo filosofo;

        for (int i = 0; i < 5; i++) {
            garfos.add(garfo = new Garfo(i));
        }

        for (int i = 0; i < 5; i++) {
            if (i == 4) {
                filosofos.add(filosofo = new Filosofo(garfos.get(4), garfos.get(0), 4));
            } else {
                filosofos.add(filosofo = new Filosofo(garfos.get(i), garfos.get(i + 1), i));
            }

        }

        ExecutorService threadExecutor = Executors.newCachedThreadPool();
        
        for (int i = 0; i < 5; i++) {
            threadExecutor.execute(filosofos.get(i));
        }

        threadExecutor.shutdown();

    }

}
