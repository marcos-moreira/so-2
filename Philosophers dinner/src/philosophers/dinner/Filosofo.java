/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package philosophers.dinner;

import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author root
 */
public class Filosofo implements Runnable {

    Garfo esq;
    Garfo dir;
    int id;

    public Filosofo(Garfo esq, Garfo dir, int id) {
        this.esq = esq;
        this.dir = dir;
        this.id = id;
    }

    public void comer() throws InterruptedException {
        System.out.println("Filoso " + id + " comendo.");
        Thread.sleep(500);
        System.out.println("Filoso " + id + " acabou de comer.");
    }

    public void pensar() throws InterruptedException {

        Random r = new Random();

        int tempo = r.nextInt(300);

        System.out.println("Filosofo " + id + " pensando por " + tempo + " milesimos.");

        Thread.sleep(tempo);

    }

    @Override
    public void run() {
        while (true) {
            try {
                pensar();

                System.out.println("Filosofo " + id + " pegando garfo esquerdo.");
                this.esq.pegar();
                System.out.println("Filosofo " + id + " pegando garfo direito.");
                this.dir.pegar();

                comer();

                System.out.println("Filosofo " + id + " liberando garfo esquerdo.");
                this.esq.devolver();
                System.out.println("Filosofo " + id + " liberando garfo direito.");
                this.dir.devolver();
            } catch (InterruptedException ex) {
                System.out.println("Deu problema.");
            }

        }
    }
}
