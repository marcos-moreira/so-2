package Example01ProducerConsumer;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class App {

    public static void main(String[] args) {
        Processor p1 = new Processor();
        ExecutorService threadExecutor = Executors.newFixedThreadPool(2);
        
        threadExecutor.execute(() -> {
            p1.produce();
        });
        
         threadExecutor.execute(() -> {
            p1.consume();
        });
         
        threadExecutor.shutdown();
    }

}
