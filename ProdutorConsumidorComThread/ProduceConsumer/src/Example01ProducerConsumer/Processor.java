package Example01ProducerConsumer;

import java.util.LinkedList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author avilapm
 */
public class Processor {
    
    private final LinkedList<Integer> list = new LinkedList<>();
    private final int LIMIT = 10;
    
    public void produce() {
        int value = 0;
        while(true){
            while(list.size() == LIMIT){
                //espero consumir algo
            }
            
            list.add(value++);
            System.out.println(Thread.currentThread().getName() + " Produziu o valor : " + value);
           
            /*try {
                sleep(500);
            } catch (InterruptedException ex) {
                Logger.getLogger(Processor.class.getName()).log(Level.SEVERE, null, ex);
            }*/
        }
    }
    
    public int consume() {
        while(true){
            System.out.print("Tamanho da lista eh: " + list.size());
            while(list.isEmpty()){
                /*Espera produzir*/
            }
            int value = list.removeFirst();
            System.out.println(" ; Consumido valor eh: " + value);
            
            /*try {
                sleep(500);
            } catch (InterruptedException ex) {
                Logger.getLogger(Processor.class.getName()).log(Level.SEVERE, null, ex);
            }*/
        }
    }
    
}
