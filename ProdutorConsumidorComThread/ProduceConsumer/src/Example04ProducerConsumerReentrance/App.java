/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Example04ProducerConsumerReentrance;
    import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
/**
 *
 * @author avilapm
 */
public class App {



   public static void main( String[] args )
   {
      // create new thread pool with two threads
      ExecutorService application = Executors.newFixedThreadPool( 2 );

      // create CircularBuffer to store ints
      Buffer sharedLocation = new CircularBuffer();

      try // try to start producer and consumer
      {
         application.execute( new Producer( sharedLocation ) );
         application.execute( new Consumer( sharedLocation ) );
      } // end try
      catch ( Exception exception )
      {
         exception.printStackTrace();
      } // end catch

      application.shutdown();
   } // end main
} // end class CircularBufferTest
