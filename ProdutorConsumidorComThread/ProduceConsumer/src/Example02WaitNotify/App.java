package Example02WaitNotify;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class App {

    public static void main(String[] args) {
        Processor p1 = new Processor();
        ExecutorService threadExecutor = Executors.newFixedThreadPool(3);

        threadExecutor.execute(() -> {
            p1.produce();
        });

        threadExecutor.execute(() -> {
            p1.consume();
        });

     
        threadExecutor.shutdown();
    }

}
