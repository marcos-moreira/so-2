package Example03ProducerConsumer;

import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javax.swing.JProgressBar;
import javax.swing.JTextArea;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author avilapm
 */
public class Processor implements Observable{
    
    private final LinkedList<Integer> list = new LinkedList<>();
    private final int LIMIT = 10;
    private final Object lock = new Object(); //monitor
    private JProgressBar barra;
    private JTextArea txtArea;
    
    public void produce() {
        int value = 0;
        while (true) {
            synchronized (lock) {
                
                while (list.size() == LIMIT) {
                    try {
                        lock.wait();
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Processor.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                
                list.add(value++);
                barra.setValue(list.size());
                lock.notify();
                txtArea.append(Thread.currentThread().getName() + " Produziu o valor : " + value+"\n");
//                System.out.println(Thread.currentThread().getName() + " Produziu o valor : " + value);
                
            }
        }
    }
    
    public int consume() {
        while (true) {
            
            synchronized (lock) {
                txtArea.append("Tamanho da lista eh: " + list.size()+"\n");
//                System.out.print("Tamanho da lista eh: " + list.size());
                
                while (list.isEmpty()) {
                    try {
                        lock.wait();
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Processor.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                int value = list.removeFirst();
                barra.setValue(list.size());
                txtArea.append(" ; Consumido valor eh: " + value+"\n");
                //System.out.println(" ; Consumido valor eh: " + value);
                lock.notify();
                
            }
        }
    }
    
    public void setPB(JProgressBar barra) {
        this.barra = barra;
    }
    
    public void setTArea(JTextArea txtArea) {
        this.txtArea = txtArea;
    }

    @Override
    public void addListener(InvalidationListener listener) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void removeListener(InvalidationListener listener) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
